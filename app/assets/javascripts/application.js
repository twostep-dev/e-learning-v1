// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery3
//= require popper
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks







$(document).ready(function(){

    // create image preview thumb when upload thumb
    readImage = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


    // create list document when upload document 
    updateList = function() {
        var input = $('#getFile')[0];
        var output = $('#fileList')[0];
        var children = "";
         $('#file_count').text(' Selected files: '+ input.files.length + ' items');
        for (var i = 0; i < input.files.length; ++i) {
                children += '<li>' + input.files.item(i).name + '</li>';
        }
        output.innerHTML = '<ul style="margin-left: 50px; margin-top: -14px;">'+children+'</ul>';
    }

    // Auto hide message after 4s
    setTimeout(function() {
        $(".alert").alert('close');
    }, 4000);

  });




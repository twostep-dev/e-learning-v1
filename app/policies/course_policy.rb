class CoursePolicy < ApplicationPolicy
    
    def index? 
        raise_error_if_unauthorize!
    end

    def show? 
    end

    def new? 
    end

    def create? 
    end

    def join? 
    end

    def destroy? 
    end

end
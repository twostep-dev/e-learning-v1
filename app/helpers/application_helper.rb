module ApplicationHelper
    def flash_class(type)
        case type
            when 'notice'
                class_name =  "alert alert-info"
            when 'success' 
                class_name =  "fa fa-check alert alert-success"
            when 'danger' 
                class_name = "fa fa-close alert alert-danger"
            when 'warning' 
                class_name = "fa fa-exclamation-triangle alert alert-warning"
        end
            class_name
    end

    def is_joined_course?(course)
        current_user.users_courses.joined(course).empty? rescue false
    end

    def is_joined_subject?(subject)
        current_user.users_subjects.joined(subject).empty? rescue false

    end


    def hash_ids(params)
        subject_ids	= params.permit!.except('utf8','action', 'controller', 'course_id', 'authenticity_token').to_h
		hash_ids = subject_ids.map { |k,v| [k.to_s, v.to_s] }.transpose.last
    end

    def build_subjects_collection(subject)
        html= []
        flag =  current_user.users_subjects.where(subject_id: subject.id).empty? ? '' : 'checked disabled' rescue ''
        html  << "<li>  <input onclick='if(this.checked){this.form.submit()}' #{flag} class='mr-2' type='checkbox' id='subject_#{ subject.id }' name='subject_#{ subject.id }' value='#{subject.id}'>#{ subject.name }</li>"
        html.first.html_safe
    end

    def percentage(course)
        caculate_data = {}
        totals =  Course.find(course.id).subjects.count
        completed = current_user.users_subjects.where(course_id: course.id).count rescue 0
        percentage = completed.to_f / (totals.zero? ? 1 : totals.to_f) * 100.0
        caculate_data.merge!('totals': totals, 'completed': completed, 'percentage': percentage.round(2))
    end


end

class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    include AuthHelper
    include ApplicationHelper


    protected
    
    def authenticate
      unless signed_in?
        store_location
        flash[:danger] = "Please login to continue learning!."
        redirect_to login_url
      end
    end 

    def pagination(record)
      Kaminari.paginate_array(record).page(params[:page]).per(6)
    end


end

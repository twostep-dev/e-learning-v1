class SubjectsController < ApplicationController
	before_action :authenticate, only: [:create,:new, :join, :destroy]
	before_action :load_resources

	def index
	end
	
	def new
	end

	def create	
		if @subject.save
			params[:subjects_documents]['documents'].each do | document |
				@subject.subjects_documents.create!(documents: document, subject_id: @subject.id)
			end
			flash[:success] = "Create subject Success !"
	    else
		 	flash[:danger] = "Something Wrong !"
		end
		redirect_to :controller => 'courses', :action => 'show', :id => @subject.course_id
	end

	def join
		if !is_joined_course?(@course)
			hash_ids(params).each do  | id |
				subject = Subject.find id
				if is_joined_subject?(subject) 
					current_user.users_subjects.create!(subject: subject, course: @course )
					flash[:success] = "Marked completed subject success !"
				end
			end
		else
			flash[:danger] = "You have't join the course!!"
		end
		redirect_to :controller => 'courses', :action => 'show', :id => params[:course_id]
	end


	def destroy
		if @subject.destroy
			flash[:success] = "Delete subject Success !"
		  else
			flash[:danger] = "Something Wrong !"
		end
		redirect_to :controller => 'courses', :action => 'show', :id => @subject.course_id
	end

	def  destroy_attachment
		if @attachment.destroy
			flash[:success] = "Delete attachment Success !"
		  else
			flash[:danger] = "Something Wrong !"
		end
		redirect_to controller: 'courses', action: 'show', id: params[:course_id]
	end

	private

	def load_resources
		case params[:action].to_sym
		when :join
		  @course = Course.find(params[:course_id])
		when :destroy
		  @subject = Subject.find(params[:id])
		when :destroy_attachment
			@attachment = SubjectsDocument.find(params[:format])
		when :create
		  @subject = Subject.new(write_params)
		when :new
			@new_course = Course.limit(3)
		end
	  end

	def write_params
		params.require(:subject).permit(
			:name,
			:time,
			:overview,
			:course_id,
			subjects_documents_attributes: [:id,:subject_id, :documents ]
		)
	end


end

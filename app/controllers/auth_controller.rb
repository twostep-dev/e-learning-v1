class AuthController < ApplicationController
    def new
    end
    
    def create
      user = User.find_by(email: params['email'].downcase)
      if user && user.authenticate(params['password_digest'])
        sign_in user
        redirect_to root_url
      else
        flash.now[:danger] = 'Invalid email/password combination' 
        render 'new'
      end
    end
    
    def destroy
        session[:user_id] = nil
        redirect_to root_url
    end
end

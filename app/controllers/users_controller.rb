class UsersController < ApplicationController
     before_action :load_resources

    def index
        @users = params.include?('in-active') ? pagination( User.only_deleted) : pagination(User.all)
    end

    def new
         @user= User.new
    end

    def edit       
    end

    def update
        if  @user.update(write_params)
            flash[:success] = "Update Profile Success"
        else
            flash.now[:danger] = @user.errors.full_messages.first
        end
        render 'edit'
    end

    def show
        @courses = pagination(current_user.users_courses.map(&:course))
    end

    def create 
        if @user.save
            sign_in @user 
            flash[:success] = "Welcome to #{@user.fullname}"
            redirect_to @user
        else

            flash.now[:danger] = @user.errors.full_messages.first
            render 'new'
        end
    end

    def destroy
        @user = User.unscoped.where(id: params[:id]).first
        if @user.deleted_at.nil?
            @user.destroy
            flash[:success] = "Block #{@user.fullname} success!"
        else
            @user.assign_attributes(deleted_at: nil)
            @user.save
            flash[:success] = "Unlock #{@user.fullname} success!"
        end
        redirect_to users_url
    end

    private


    def load_resources
      case params[:action].to_sym
      when :index, :show, :destroy
          @new_course = Course.limit(3)
      when :edit, :update
          @user = User.unscoped.where(id: params[:id]).first
      when :create
          @user = User.new(write_params)
      end
    end

    def write_params
        params.require(:user).permit(
          :fullname,
          :email,
          :password,
          :birthday,
          :avatar_url,
          :address,
          :role
        )
    end

end

class CoursesController < ApplicationController
  before_action :authenticate, only: [:create,:new,:destroy,:join]
  before_action :load_resources

  def index
    if params[:search].present?
      @courses = pagination(Course.search(params[:search]))

    else
      @courses = pagination(Course.by_category(params[:condition]))
    end
  end
  
  def show
  end

  def new
  end

  def create
	 if @course.save
      flash[:success] = "Create Success !"
      redirect_to root_path      
    else
      flash[:danger] = "#{@course.errors.full_messages.first}"
      render  'new'
    end
	end

  def join
    if is_joined_course?(@course)
      current_user.users_courses.create!(course: @course)
      flash[:success] = "Wellcome to join with us!"
    else
      current_user.users_courses.joined(@course).delete_all
      current_user.users_subjects.where(course_id: @course.id).delete_all    
      flash[:danger] = "Leave Success!"
    end
    redirect_to  action: 'show', id: @course.id

  end

  def destroy
    if @course.destroy
      flash[:success] = "Delete Success !"
    else
      flash[:danger] = "Something Wrong !"
    end
      redirect_to root_path
  end

  private

  def load_resources
    case params[:action].to_sym
    when :index, :new
      @new_course = Course.limit(3)
    when :join, :show, :destroy
      @course = Course.find(params[:id])
      is_joined_course?(@course)
    when :create
      @course = current_user.courses.new(write_params)
    end
  end


	def write_params
    params.require(:course).permit(
      :title,
      :category,
      :thumb_url,
      :price,
      :description
    )
  end

end

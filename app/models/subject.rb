# == Schema Information
#
# Table name: subjects
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  name       :string
#  overview   :string
#  time       :integer
#  created_at :datetime
#  updated_at :datetime
#  course_id  :integer
#
class Subject < ApplicationRecord
    has_many :users_subjects
    has_many :subjects_documents, dependent: :destroy
    belongs_to :course

    accepts_nested_attributes_for :subjects_documents

end

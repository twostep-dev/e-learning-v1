# == Schema Information
#
# Table name: users_subjects
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  created_at :datetime
#  updated_at :datetime
#  course_id  :integer
#  subject_id :integer
#  user_id    :integer
#
class UsersSubject < ApplicationRecord
	scope :by_course_id, lambda{ |course_id| where(course_id: course_id) }
	# scope :joined, lambda{ |subject| where(course_id: subject.id,user_id: User.current.id)}
	scope :joined, lambda{ |subject| where(subject_id: subject.id)}


	belongs_to :user
	belongs_to :subject
	belongs_to :course
end

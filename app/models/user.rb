# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  address         :string
#  avatar_url      :string
#  birthday        :datetime
#  deleted_at      :datetime
#  email           :string           not null
#  fullname        :string
#  password_digest :string           not null
#  role            :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#
class User < ApplicationRecord

    has_secure_password
    validates :email, presence: true, uniqueness: true
    validates :password_digest, presence: true

    has_many :courses, dependent: :destroy

    has_many :users_courses
    has_many :users_subjects
  
    enum role: {
	user: 0,
	admin: 10
    }

end

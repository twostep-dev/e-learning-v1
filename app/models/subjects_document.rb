# == Schema Information
#
# Table name: subjects_documents
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  documents  :string
#  created_at :datetime
#  updated_at :datetime
#  subject_id :integer
#
class SubjectsDocument < ApplicationRecord
  belongs_to :subject
  mount_uploader  :documents, FilesUploader

end

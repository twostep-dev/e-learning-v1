# == Schema Information
#
# Table name: users_courses
#
#  id         :bigint           not null, primary key
#  deleted_at :datetime
#  created_at :datetime
#  updated_at :datetime
#  course_id  :integer
#  user_id    :integer
#
class UsersCourse < ApplicationRecord
	scope :joined, lambda{ |course| where(course_id: course.id)}

	belongs_to :user
	belongs_to :course
end

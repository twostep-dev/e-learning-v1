# == Schema Information
#
# Table name: courses
#
#  id          :bigint           not null, primary key
#  category    :string
#  deleted_at  :datetime
#  description :string
#  price       :integer
#  thumb_url   :string
#  title       :string
#  created_at  :datetime
#  updated_at  :datetime
#  user_id     :integer
#
class Course < ApplicationRecord
    default_scope { order(created_at: :desc) }

    scope :by_created_at, lambda{ order(created_at: :desc).limit(3) }
    scope :by_category, lambda{ |category| where(category: category)  if category.present?  }
    scope :search, lambda{ |keyword| where("title LIKE '%#{keyword}%' ")  if keyword.present?  }

    has_many :users_courses
    has_many :users_subjects
    has_many :subjects, dependent: :destroy

    belongs_to :user
    
    mount_uploader :thumb_url, FilesUploader

    validates :title, presence: true
    validates :price, presence: true
    validates :thumb_url, presence: true


end


module ActionView
    module Helpers
        # def form_with(options)
        #     options[:local] = true
        #     super options
        # end

        def form_with(model: nil, scope: nil, url: nil, format: nil, **options)
            options[:allow_method_names_outside_object] = true
            options[:skip_default_ids] = true
            options[:local] = true
    
            if model
              url ||= polymorphic_path(model, format: format)
    
              model   = model.last if model.is_a?(Array)
              scope ||= model_name_from_record_or_class(model).param_key
            end
    
            if block_given?
              builder = instantiate_builder(scope, model, options)
              output  = capture(builder, &Proc.new)
              options[:multipart] ||= builder.multipart?
    
              html_options = html_options_for_form_with(url, model, options)
              form_tag_with_body(html_options, output)
            else
              html_options = html_options_for_form_with(url, model, options)
              form_tag_html(html_options)
            end
        end

    end
end
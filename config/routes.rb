Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  resources :courses do
    member do
      post 'join', action: :join
    end
  end

  resources :subjects do
    collection do
      post 'join', action: :join
      delete 'destroy_attachment', action: :destroy_attachment
    end
  end

  resources :users
  resources :subjects_documents, :only => [:destroy]


  root controller: :courses, action: :index
  
  get '/register', to: 'users#new' 
  post '/register', to: 'users#create' 

  get    '/login',   to: 'auth#new'   
  post   '/login',   to: 'auth#create'   
  delete '/logout',  to: 'auth#destroy'

end

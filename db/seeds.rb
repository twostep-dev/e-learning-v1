require 'faker'



User.create(
    fullname: 'ADMIN',
    role: 'admin',
    email: 'admin@example.com',
    password_digest: '$2a$12$XhplZTvfU4mwmgE.41aGa.Yov0b6hlv9SkWr5mYxA.If.t6MvdPWq'  
)


14.times do |index|
  User.create(
    fullname: "USER_0#{index}",
    role: "user",
    email: "user_0#{index}@example.com",
    password_digest: '$2a$12$XhplZTvfU4mwmgE.41aGa.Yov0b6hlv9SkWr5mYxA.If.t6MvdPWq'  
  )
end


#  User.delete_all && Course.delete_all &&  Subject.delete_all  && SubjectsDocument.delete_all 


Rails.logger.info("--------------------FINISHED  CREATE USERS----------------------")

30.times do 
    Course.create(
      title: Faker::ProgrammingLanguage.name + " - " + Faker::ProgrammingLanguage.creator,
      description: Faker::Lorem.paragraph_by_chars(number: 800, supplemental: false),
      price: rand(1000),
      category: ['IT - Software', 'Photography', 'Programming', 'Technology'].sample,
      user_id: User.last.id,
      thumb_url: File.open(File.join(Rails.root, '/test/mock_data/thumbs', "#{[*1..14].sample}.jpg"))
    )
  end

  Rails.logger.info("--------------------FINISHED  CREATE COURSES----------------------")


  200.times do 
    Subject.create(
      name: Faker::TvShows::VentureBros.quote,
      time: rand(50),
      course_id: Course.ids.sample
    )
  end

  Rails.logger.info("--------------------FINISHED  CREATE SUBJECTS----------------------")

 
  200.times do 
    SubjectsDocument.create(
      documents: File.open(File.join(Rails.root, '/test/mock_data/documents', "document_0#{[*1..7].sample}.txt")),
      subject_id: Subject.ids.sample
    )
  end

  Rails.logger.info("--------------------FINISHED  CREATE DOCUMENT----------------------")


  200.times do 
    UsersCourse.create(
      course_id: Course.ids.sample,
      user_id: User.ids.sample
    )
  end

  Rails.logger.info("--------------------FINISHED  JOINS USER CCOURSE----------------------")

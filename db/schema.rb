# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_11_061425) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "category"
    t.integer "price"
    t.integer "user_id"
    t.string "thumb_url"
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.datetime "deleted_at"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.integer "time"
    t.string "overview"
    t.integer "course_id"
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.datetime "deleted_at"
  end

  create_table "subjects_documents", force: :cascade do |t|
    t.string "documents"
    t.integer "subject_id"
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.datetime "deleted_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "fullname"
    t.datetime "birthday"
    t.string "address"
    t.string "avatar_url"
    t.integer "role", null: false
    t.string "email", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.datetime "deleted_at"
  end

  create_table "users_courses", force: :cascade do |t|
    t.integer "course_id"
    t.integer "user_id"
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.datetime "deleted_at"
  end

  create_table "users_subjects", force: :cascade do |t|
    t.integer "user_id"
    t.integer "subject_id"
    t.integer "course_id"
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.datetime "deleted_at"
  end

end

class UsersSubjects < ActiveRecord::Migration[6.0]
  def change
    create_table :users_subjects, force: :cascade do |t|
        # Detail
        t.integer :user_id
        t.integer :subject_id
        t.integer :course_id


        # Uncomment below if timestamps were not included in your original model.
        t.timestamps null: true
        t.datetime :deleted_at
  
    end
  end
end

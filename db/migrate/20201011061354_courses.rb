class Courses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses, force: :cascade do |t|
      # Detail
      t.string :title
      t.string :description
      t.string :category
      t.integer :price
      t.integer :user_id
      t.string :thumb_url
      # Uncomment below if timestamps were not included in your original model.
      t.timestamps null: true
      t.datetime :deleted_at
    end
  end
end

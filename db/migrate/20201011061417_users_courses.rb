class UsersCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :users_courses, force: :cascade do |t|
        # Detail
        t.integer :course_id
        t.integer :user_id

        # Uncomment below if timestamps were not included in your original model.
        t.timestamps null: true
        t.datetime :deleted_at
    end
  end
end

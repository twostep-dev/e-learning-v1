class Subjects < ActiveRecord::Migration[6.0]
  def change
    create_table :subjects, force: :cascade do |t|
      # Detail
      t.string :name
      t.integer :time
      t.string :overview
      t.integer :course_id

      # Uncomment below if timestamps were not included in your original model.
      t.timestamps null: true
      t.datetime :deleted_at
  
    end
  end
end

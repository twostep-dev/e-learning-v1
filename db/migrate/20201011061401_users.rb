class Users < ActiveRecord::Migration[6.0]
  def change
    create_table :users, force: :cascade do |t|
      # Detail
      t.string :fullname
      t.datetime :birthday
      t.string :address
      t.string :avatar_url
      t.integer :role, null: false, default: 'user'

      t.string "email", null:false
      t.string "password_digest",    null: false
      
      # Uncomment below if timestamps were not included in your original model.
      t.timestamps null: true
      t.datetime :deleted_at
  
    end
  end
end

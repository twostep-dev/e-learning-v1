class SubjectsDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :subjects_documents, force: :cascade do |t|
      # Detail
      t.string :documents
      t.integer :subject_id

      # Uncomment below if timestamps were not included in your original model.
      t.timestamps null: true
      t.datetime :deleted_at
    end
  end
end
